<?php require('header.php'); ?>
<main>
    <?php include('header-top.php'); ?>

<?php
require_once('lib/var.php');
require_once('lib/form_traitement.php');

// $country = json_decode('http://immo-lead.com/api/1/commons?apikey=1ee81-10cb1-c32b8-79740-8043d-75202-7f&customerkey=8f42b-d3a42-3ae73-89dcd-29a1e-0bdfc-10&action=getPays');



$resultPays = json_decode(file_get_contents('http://immo-lead.com/api/1/commons?apikey=1ee81-10cb1-c32b8-79740-8043d-75202-7f&customerkey=8f42b-d3a42-3ae73-89dcd-29a1e-0bdfc-10&action=getPays'),true);


 // print_r($resultPays);

?>


<style>
  .success{
    display: none;
  }
  #pays{
    padding-left: 50px;
    width: 275px;
    padding-right: 10px;
  }

  .popuprcs, .popupmail, .popupisn, .popupisnl{
    cursor: pointer;
    width: 333px;
    height: auto;
    border: solid 1px red;
    position: absolute;
    background: white;
    opacity: 1;
    padding: 11px;
    left: 41%;
    top: 10px;
    font-size: 18px;
    display: none;
  }

  .hide{
    display: none;
  }

  .add{
    background-image: url(img/add.png);
    width: 150px;
    height: 50px;
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
  }
    .multinput{
      width: 71%;
      padding: 10px;
      box-shadow: 2px 2px 2px 2px #0000005e;
      background-color: #ffffff12;
    }
</style>
<div class="popupmail">
  <p>Email déjà existant, inserer une nouvelle adresse email ou rendez-vous sur :<br>
  <a href="http://immo-lead.com" style="color: black;font-weight: bold;">http://immo-lead.com</a></p>
</div>
<div class="popuprcs">
  <p>Votre compte existe déjà. Pour vous connecter, rendez-vous ici :<br> <a href="http://immo-lead.com" style="color: black;font-weight: bold;">http://immo-lead.com</a> N'hésitez pas à contacter notre service à cette adresse <a href="mailto:partenaires@spirit.net" style="color: black;font-weight: bold;">partenaires@spirit.net</a> pour toutes informations complémentaires.</p>
</div>
<div class="popupisn">
  <p>Le SIRET doit etre un nombre ENTIER</p>
</div>
<div class="popupisnl">
  <p>Le SIRET doit être composé de 14 chiffres sans espace</p>
</div>
<?php if(!empty($_POST)){ ?>
       <!-- POP IN -->
       <style>
         .success{
          display: unset;
         }
       </style>
<div class="popContact ct success">
  <div class="containerModal">
        <p>Votre compte est créé ! Spirit est heureux de vous accueillir parmi ses partenaires.<br>
Vous retrouverez votre identifiant et mot de passe par email après validation.<br> N'hésitez pas à contacter notre service à cette adresse <strong>partenaires@spirit.net</strong> pour toutes informations complémentaires.</p></p>
          <div style="position: absolute;top: 0;bottom: 0;left: 0;right: 0;z-index: 10000000000000000000000000;">

          </div>
    </div>
</div>
<!-- / POP IN -->
<?php } ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css"></script>

    <div class="containerForm">

      <form id="form" method="POST" data-step="1" enctype='multipart/form-data'>
      <fieldset class="etape" data-step="1">
        <legend class="devenezP">Devenez partenaire</legend>
        <h2>Devenez partenaire en 2 étapes</h2>
        <h3>étape - 1</h3>
        <div class="etape1">
          <h4>Informations générales</h4>
          <p>(Identifiez-vous)</p>
        </div>
        <h3>étape - 2</h3>
        <div class="etape2">
          <h4>Informations particulières</h4>
          <p>(Nécessaire à l'établissement des mandats)</p>
        </div>
        <button type="button" id="nextBtn1" class="btns1 ctaSubscribes btnRed toRed">Suivant</button>
      </fieldset>

      <fieldset class="step1" data-step="2">
        <legend>Informations de la société 1/2 </legend>

        <div class="blocForm">
          <div class="divLabel">
            <label for="lastname">Nom* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="lastname" name="nomprescripteur">
          </div>
          <p id="errorln">Ce champ ne doit pas dépasser 55 caractères</p>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="societe">Type de société* :</label>
          </div>
          <div class="divInput">
            <select style="width: 271px;" id="societe" name="societe" >
              <option value="">Aucune</option>
              <option value="4">EURL</option>
              <option value="3">SARL</option>
              <option value="2">SA</option>
              <option value="6">SCA</option>
              <option value="5">SCS</option>
              <option value="7">SNC</option>
              <option value="8">SE</option>
              <option value="9">SAS</option>
              <option value="10">SASAU</option>
              <option value="1">SASU</option>
            </select>
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="rcs">Immatriculée au RCS de* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="rcs2" name="rcs2" >
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="rcs">Sous le n° SIRET* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="rcs" name="siret">
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="capital">Capital* :</label>
          </div>
          <div class="divInput">
            <input type="number" id="capital" name="capital" >
          </div>
        </div>

        

        <div class="blocForm">
          <div class="divLabel">
            <label for="adresse">Adresse* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="adresse" name="adresse">
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="cp">Code Postal* :</label>
          </div>
          <div class="divInput">
            <input type="number" id="cp" name="cp">
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="ville">Ville* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="ville" name="ville">
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="pays">Pays* :</label>
          </div>
          <div class="divInput">
            <select id="pays" name="pays">
                <option value="">Sélectionner un pays</option>
              <?php foreach ($resultPays as $kPays => $valPays) { ?>
                <option value="<?php echo $valPays['libelle']; ?>"><?php echo $valPays['libelle']; ?></option>
              <?php } ?>
            </select>
            <!-- <input type="text" id="pays" nom="pays" placeholder=" France"> -->
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="email">Email* :</label>
          </div>
          <div class="divInput">
            <input required="required" type="email" id="email" name="email">
            <input type="hidden" name="email2" class="hidden">
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="phone">Téléphone* :</label>
          </div>
          <div class="divInput">
            <input type="tel" id="phone" name="phone" minlength="8" maxlength="20">
          </div>
          <p id="errornu">Ce champ ne doit pas dépasser 55 caractères</p>
        </div>  
        <div class="blocForm">
          <div class="divLabel">
            <label for="ncp">Numéro de carte professionnelle* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="ncp" name="ncp"  maxlength="55">
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="deliver">Délivrée le* :</label>
          </div>
          <div class="divInput">
            <input type="date" id="deliver" name="deliver" style="width: 272px;text-align: center;font-size: 13px;">
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="prefecture">Par la préfecture de* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="prefecture" name="prefecture">
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="siret">Organisme de la garantie bancaire :</label>
          </div>
          <div class="divInput">
            <input type="text" id="garantie_bancaire" name="garantie_bancaire" style="    width: 78%;">
          </div>
        </div>

         <div class="blocForm">
          <div class="divLabel">
            <label for="ncp">Kbis* :</label>
          </div>
          <div class="divInput">
            <input type="file" id="fileToUploadkbis" name="fileToUpload[]" >
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="ncp">Carte T* :</label>
          </div>
          <div class="divInput">
            <input type="file" id="fileToUploadct" name="fileToUpload[]" >
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="ncp">Attestation de garantie* :</label>
          </div>
          <div class="divInput">
            <input type="file" id="fileToUploadag" name="fileToUpload[]" >
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="ncp">Assurance* :</label>
          </div>
          <div class="divInput">
            <input type="file" id="fileToUploada" name="fileToUpload[]" >
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="ncp">Carte d'identité gérant* :</label>
          </div>
          <div class="divInput">
            <input type="file" id="fileToUploadcig" name="fileToUpload[]" >
          </div>
        </div>

        <div class="prevNext">
          <button type="button" class="ctaSubscribes btnRed toRed prev button button-outline">Retour</button>
          <button type="button"  class="ctaSubscribes btns2 btnRed toRed next button2">Continuez</button>
        </div>
        <p>Champs obligatoire (*)</p>
      </fieldset>



      <fieldset class="step2" data-step="3">
        <legend>Informations du gérant 2/2 </legend>

        <div class="blocForm">
          <div class="divLabel">
            <label for="nom">Nom du gérant* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="nom[]" name="nom[]" >
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="firstname">Prénom du gérant* :</label>
          </div>
          <div class="divInput">
            <input type="text" id="firstname[]" name="firstname[]" >
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel">
            <label for="email3">Email du gérant* :</label>
          </div>
          <div class="divInput">
            <input type="email" id="email3" name="email3[]">
          </div>
        </div>

        <div class="blocForm">
          <div class="divLabel">
            <label for="mobile">Portable du gérant* :</label>
          </div>
          <div class="divInput">
            <input type="tel" id="mobile" name="mobile" >
          </div>
        </div>
        <div class="blocForm">
          <div class="divLabel dlch">
            <fieldset style="    width: 100%;  padding: 10px;">
                <legend>Informations relatives aux lancements commerciaux </legend>
                <input type="checkbox" name="direction[]" value="Ile-de-France" id="idf" />
                <label style="padding-left: 5px;" for="idf"> Ile-de-France</label><br />
                <input type="checkbox" name="direction[]" value="Alsace" id="alsace"  />
                <label style="padding-left: 5px;" for="alsace">Alsace</label><br />
                <input type="checkbox" name="direction[]" value="Rhône-Alpes" id="ra" />
                <label style="padding-left: 5px;" for="ra">Rhône-Alpes</label><br />
                <input type="checkbox" name="direction[]" value="Azur" id="azur" />
                <label style="padding-left: 5px;" for="azur">Azur</label><br />
                 <input type="checkbox" name="direction[]" value="Provence" id="provence" />
                 <label style="padding-left: 5px;" for="provence">Provence</label><br />
            </fieldset>
          </div>
        </div>
        <div class="mul">
          <div class="blocForm multinput">
            <div class="divLabel">
              <label for="emailchimp">Abonner un collaborateur :</label>
            </div>
            <div class="divInput">
              <input type="email" name="email3[]" placeholder="exemple.email@spirit.fr">
                 <input class="secondch" type="text" name="nom[]" placeholder="Nom">
              <input class="secondch" type="text" name="firstname[]" placeholder="Prenom">
            </div>
         </div>
        </div>
       <div class="add"> </div>

        <div class="prevNext">
          <button type="button" class="ctaSubscribes btnRed toRed prev button button-outline">Retour</button>
          <button type="button" class="ctaSubscribes btns3 btnRed toRed">Envoyer</button>
        </div>
        <p>Champs obligatoire (*)</p>

      </fieldset>

      </form>

    </div>

</main>
<?php require('footer.php'); ?>

<div style="position: absolute;left: -9999px;">
 <p id="bobscs">0</p>
</div>

<script>

</script>
