<footer>
    <a href="mentions-legales" target="_blank">Mentions légales</a>
    <a href="http://www.spirit-immobilier.fr/" target="_blank">www.spirit-immobilier.fr</a>
</footer>

<!--jquery-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!--caroussel-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<!--end caroussel-->
<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>
