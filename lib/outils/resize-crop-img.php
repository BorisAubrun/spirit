<?php

if(!empty($_GET['src']) && !empty($_GET['w']) && !empty($_GET['h'])){

    header('Content-Type: image/jpeg');

    // source de l'image
    $source = $_GET['src'];

    switch ( strtolower(substr($_GET['src'], -4))) {
        case 'jpeg':
            header('Content-Type: image/jpeg');
            break;
        case '.jpg':
            header('Content-Type: image/jpeg');
            break;
        case 'png':
            header('Content-Type: image/png');
            break;
        default:
            header('Content-Type: image/jpeg');
            break;
    }

    // Définition de la largeur et de la hauteur maximale
    $newWidth = $_GET['w'];
    $newHeight = $_GET['h'];

    // récupération des dimension de l'image source
    list($widthOrig, $heightOrig) = getimagesize($source);
	
	$ratioWidth = $widthOrig/$newWidth;
	$ratioHeight = $heightOrig/$newHeight;
	
	$x = $y = 0;
	if($ratioWidth < $ratioHeight){
		$nWidth = $widthOrig;
		$nHeight = $newHeight * $ratioWidth;
		$y = ($heightOrig - $nHeight) / 2;
	}else{
		$nHeight = $heightOrig;
		$nWidth = $newWidth * $ratioHeight;
		$x = ($widthOrig - $nWidth) / 2;
	}

    // création de l'image à partir de la source
    $imSource = imagecreatefromjpeg($source);
    
	$imSource2 = imagecrop($imSource, ['x' => $x, 'y' => $y, 'width' => $nWidth, 'height' => $nHeight]);
   
	// création d'une image vide
    $imDestination=imagecreatetruecolor($newWidth,$newHeight);

    // copie de l'image avec redimensionnement
    imagecopyresampled ($imDestination,$imSource2,0,0,0,0,$newWidth,$newHeight,$nWidth,$nHeight);
	
    // Affichage
    // imagejpeg($imSource2);
    imagejpeg($imDestination);
    imagedestroy($source);
}
?>