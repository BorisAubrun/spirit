<?php

/*
getInfosProgrammesClientImmolead: Récupère toutes les informations porgrammes du client (hors lots / médias)
getProgrammeClientByDate : 		Tri les programme par date
getInfosProgrammesImmolead : 	Récupère toutes les informations programmes
getInfosLotsImmolead : 			Récupère toutes les informations lots
getInfosMediasImmolead :		Récupère tous les médias
getInfosPackagesImmolead :		Récupère tous les packages
getProgrammeInvest :			Récupère les programmes duflot / pinel à 1
getProgrammeVilles : 			Récupère les villes des programmes
getLotsDispoByTypo :			
getPrixAPartirDe :				Récupère les prix des lots dispos à partir de par typologie
getPrixAPartirDeByType : 		Récupère les prix des lots dispos à partir de par type puis par typologie
getAllPrixAPartirDe :			Récupère les prix des lots à partir de par typologie
getSurfaceAPartirDe :			Récupère les surface des lots à partir par typologie
getSurfaceMiniMaxi :			Récupère les surfaces mini et maxi par typologies
getPhraseAllPrixAPartirDe :		Récupère une phrase des prix à partir de. Ex du studio au 5 pièces
getPrixMiniByProgramme :		Récupère le prix mini du programme
getPrgsLots :					Récupère le flux programmes avec le flux lots brutes mixé
getNbreLogementDispo :			Récupère le nombre de logement dispo
getNbreLogement :				Récupère le nombre de logement total
getNbreLogementDispoByTypo :	Récupère le nombre de logement dispo par typologie
getNbreLogementByTypo :			Récupère le nombre de logement par typologie
getPrgs : 						Récupération des programmes brutes
getLots :						Récupération des lots brutes
getDataLotsDispoByTypo : 		Récupère les données des lots par typologie
getPlanLot :					Récupère les plans des lots
getVisiteVirtuelle :			Récupère les visites virtuelles
getPackageLot :					Récupère les packages des lots
getLotByIdlot : 				Tri les lots des informations lots par idlot
getRawPrgs :					Récupère les programmes brutes du client
getInfosNodes : 				Récupération d'une table de correspondance
askNearProgramme :				Demande les programmes à proximité en km
getNearProgrammes : 			Récupère les programmes
*/

class Immolead
{
	private $urlApiClientProgramme = 'api-programme.php';
	private $urlApiProgramme = 'programmes-simple';
	private $urlApiLot = 'lots-simple';
	private $urlApiMedia = 'media-simple';
	private $urlApiPackage = 'package-simple';
	private $urlApiContacts = 'contacts';
	private $urlApiNodes = 'get-nodes';
	private $urlApiRayonnage = 'get-rayonnage';
	private $urlApiUpload = 'uploads';
	private $typoFlux = array("1","2","3","4","5","6");
	private	$typoFrance = array("Studio","2 pièces","3 pièces","4 pièces","5 pièces","6 pièces");
	private $idMediaplan = 1;
	private $idMediaplan2d = 1;
	private $idMediaInteractif = 4;
	private $idMediaVisiteVirtuelle = 1;

	public function __construct()
	{
		$ProgrammeClient = array();
		$Programme = array();
		$Lots = array();
		$Medias = array();
		$Packages = array();
		$nearProgrammes = array();
		$ProgrammePinel = array();
		$ProgrammeVilles = array();
		$LotsDispoByProgramme = array();
		$PrixMiniProgramme = array();
		$PrixAPrtirDe = array();
		$PrixAPrtirDeByType = array();
		$allprixAPrtirDe = array();
		$surfaceAPrtirDe = array();
		$surfaceMiniMaxi = array();
	}

	// Récupère toutes les informations programmes d'une client (hors lots / médias)
	function getInfosProgrammesClientImmolead(){
		$url_api_prg = URLIMMOLEAD.'api/'.$this->urlApiClientProgramme.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY;
		$content = file_get_contents($url_api_prg);
		$content = json_decode($content,true);
		$this->ProgrammeClient = $content;
	}

	// Récupére toutes les informations programme d'Immolead 
	function getInfosProgrammesImmolead($idproduit=array()){
		
		$url_api_prg = URLIMMOLEAD.'api/1/'.$this->urlApiProgramme.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY.'';
		if(!empty($idproduit)){
			foreach($idproduit as $key => $value){
				$url_api_prg.= '&multiidproduit[]='.$value;
			}
		}
		$content = file_get_contents($url_api_prg);
		$content = json_decode($content,true);
		$this->Programme = $content;
	}

	// Récupére toutes les informations lots d'Immolead 
	function getInfosLotsImmolead($idproduit=array(),$typologie=array(),$typetypo=array(),$multidiff=null,$batiment=null){

		$url_api_prg = URLIMMOLEAD.'api/1/'.$this->urlApiLot.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY.'';
		if(!empty($idproduit)){
			foreach($idproduit as $key => $value){
				$url_api_prg.= '&multiidproduit[]='.$value;
			}
		}
		if(!empty($typologie)){
			foreach($typologie as $key => $value){
				$url_api_prg.= '&multitypo[]='.$value;
			}
		}
		if(!empty($typetypo)){
			foreach($typetypo as $key => $value){
				$url_api_prg.= '&multitypetypo[]='.$value;
			}
		}
		if($multidiff==1){
			$url_api_prg.= '&multidiff='.$multidiff;
			
		}
		if(!empty($batiment)){
			$url_api_prg.= '&batiment='.$batiment;
			
		}
		$content = file_get_contents($url_api_prg);
		$content = json_decode($content,true);
		$this->Lots = $content;

	}

	// Récupére toutes les informations médias d'Immolead 
	function getInfosMediasImmolead($idproduit=array()){
		
		$url_api_prg = URLIMMOLEAD.'api/1.2/'.$this->urlApiMedia.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY.'';
		if(!empty($idproduit)){
			foreach($idproduit as $key => $value){
				$url_api_prg.= '&multiidproduit[]='.$value;
			}
		}
		$content = file_get_contents($url_api_prg);
		$content = json_decode($content,true);
		$this->Medias = $content;
	}

	// Récupére toutes les informations packages d'Immolead 
	function getInfosPackagesImmolead($idproduit=array()){
		
		$url_api_prg = URLIMMOLEAD.'api/1/'.$this->urlApiPackage.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY.'';
		if(!empty($idproduit)){
			foreach($idproduit as $key => $value){
				$url_api_prg.= '&multiidproduit[]='.$value;
			}
		}
		$content = file_get_contents($url_api_prg);
		$content = json_decode($content,true);
		$this->Packages = $content;
	}

	// Récupére toutes les informations programme d'Immolead 
	function getInfosNodes($type=null){
		
		$url_api_node = URLIMMOLEAD.'api/1/'.$this->urlApiNodes.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY.'&action=getnode';
		if(!empty($type)){
			$url_api_node.= '&tab='.$type;
		}
		$nodes = file_get_contents($url_api_node);
		return $nodes;
	}

	// Tri par idlot
	function getLotByIdlot(){
		$lotsByIdlot = array();
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$lotsByIdlot[$lots['idlot']] = $lots;
		}
		return $lotsByIdlot;
	}

	// Récupère les programme classé par date
	function getProgrammeClientByDate(){
		$programmesTri = array();
		foreach($this->ProgrammeClient['data']['programmes'] as $key =>$programme){
			$dateenr = str_replace('-', '', $programme['infoprog']['dateenr']);
			$programmesTri[$dateenr][] = $programme;
		}
		krsort($programmesTri);
		return $programmesTri;
	}

	// Filtre le programmes Invest (Pinel dans Immolead, duflot dans le flux)
	function getProgrammeInvest(){
		foreach($this->Programme['programmes']['data'] as $key =>$programme){
			if($programme['duflot']==1){
				$this->ProgrammePinel[$programme['idproduit']] = 1;
			}
		}
		return $this->ProgrammePinel;
	}

	// Récupération des villes des programmes appelés
	function getProgrammeVilles(){
		foreach($this->Programme['programmes']['data'] as $key =>$programme){
				$this->ProgrammeVilles[$programme['idproduit']] = $programme['villeprg'];
		}
		return $this->ProgrammeVilles;
	}

	// Filtre les lots libre par typologies
	function getLotsDispoByTypo(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if($lots['statu_lot']=='libre'){
				$this->LotsDispoByProgramme[$lots['idprg']]['typologies'][$lots['typologie']][] = 1;
			}
		}
		return $this->LotsDispoByProgramme;
	}

	// Données des lots libre par typologies
	function getDataLotsDispoByTypo(){
		$dataLotsByTypo = array();
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if($lots['statu_lot']=='libre'){
				$this->dataLotsByTypo[$lots['idprg']]['typologies'][$lots['typologie']][$lots['idlot']] = $lots;
			}
		}
		return $this->dataLotsByTypo;
	}

	// Filtre les lots libres et les classe par typologies prix à partir de 
	function getPrixAPartirDe(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$idproduit = $lots['idprg'];
			$typo = $lots['typologie'];
			$prix = $lots['prix'];
			if($lots['statu_lot']=='libre'){
				if(empty($this->PrixAPrtirDe[$idproduit]['typologies'][$typo]) 
					OR $prix<$this->PrixAPrtirDe[$idproduit]['typologies'][$typo]
				){
					$this->PrixAPrtirDe[$idproduit]['typologies'][$typo] = $prix;
				}
			}
		}
		return $this->PrixAPrtirDe;
	}

	// Filtre les lots libres et les classe par type puis par typologies prix à partir de 
	function getPrixAPartirDeByType(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$idproduit = $lots['idprg'];
			$typo = $lots['typologie'];
			$type = $lots['typetypo'];
			$prix = $lots['prix'];
			if($lots['statu_lot']=='libre'){
				if(empty($this->PrixAPrtirDeByType[$idproduit]['type'][$type][$typo]) 
					OR $prix<$this->PrixAPrtirDeByType[$idproduit]['type'][$type][$typo]
				){
					$this->PrixAPrtirDeByType[$idproduit]['type'][$type][$typo] = $prix;
				}
			}
		}
		return $this->PrixAPrtirDeByType;
	}

	// Classe tous les lots par typologies prix à partir de 
	function getAllPrixAPartirDe(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$idproduit = $lots['idprg'];
			$typo = $lots['typologie'];
			$prix = $lots['prix'];
			if(empty($this->allprixAPrtirDe[$idproduit]['typologies'][$typo]) 
				OR $prix<$this->allprixAPrtirDe[$idproduit]['typologies'][$typo]
			){
				$this->allprixAPrtirDe[$idproduit]['typologies'][$typo] = $prix;
			}
		}
		return $this->allprixAPrtirDe;
	}

	// Filtre les lots libres et les classe par typologies surface à partir de 
	function getSurfaceAPartirDe(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$idproduit = $lots['idprg'];
			$typo = $lots['typologie'];
			$surface = $lots['surface'];
			if($lots['statu_lot']=='libre'){
				if(empty($this->surfaceAPrtirDe[$idproduit]['typologies'][$typo]) 
					OR $surface<$this->surfaceAPrtirDe[$idproduit]['typologies'][$typo]
				){
					$this->surfaceAPrtirDe[$idproduit]['typologies'][$typo] = $surface;
				}
			}
		}
		return $this->surfaceAPrtirDe;
	}

	function getSurfaceMiniMaxi(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$idproduit = $lots['idprg'];
			$typo = $lots['typologie'];
			$surface = $lots['surface'];
			if($lots['statu_lot']=='libre'){
				if(empty($this->surfaceMiniMaxi[$idproduit]['typologies'][$typo]['mini']) 
					OR $surface<$this->surfaceMiniMaxi[$idproduit]['typologies'][$typo]['mini']
				){
					$this->surfaceMiniMaxi[$idproduit]['typologies'][$typo]['mini'] = $surface;
				}

				if(empty($this->surfaceMiniMaxi[$idproduit]['typologies'][$typo]['maxi']) 
					OR $surface>$this->surfaceMiniMaxi[$idproduit]['typologies'][$typo]['maxi']
				){
					$this->surfaceMiniMaxi[$idproduit]['typologies'][$typo]['maxi'] = $surface;
				}
			}
		}
		return $this->surfaceMiniMaxi;
	}

	// Phrases des typo des lots dispo 
	function getPhraseAllPrixAPartirDe(){
		$phraseTypo = array();
		foreach($this->allprixAPrtirDe as $idproduit => $typologies){
			if(!empty($typologies['typologies'])){
				ksort($typologies['typologies']);
				$typologies['typologies'] = array_flip($typologies['typologies']);

				$first = current($typologies['typologies']);
				$end = end($typologies['typologies']);

				$first = str_replace($this->typoFlux, $this->typoFrance, $first);
				$end = str_replace($this->typoFlux, $this->typoFrance, $end);

				if($first!=$end){
					$phraseTypo[$idproduit] = 'du '.$first.' au '.$end;
				}else{
					$phraseTypo[$idproduit] = $first;
				}
			}else{
				$phraseTypo[$idproduit] = '';
			}
		}

		return $phraseTypo;
	}


	// Retourne le prix mini du programme des lots libres
	function getPrixMiniByProgramme(){
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if($lots['statu_lot']=='libre'){
				if(empty($this->PrixMiniProgramme[$lots['idprg']]['prix']) 
					OR $lots['prix']<$this->PrixMiniProgramme[$lots['idprg']]['prix']
				){
					$this->PrixMiniProgramme[$lots['idprg']]['prix'] = $lots['prix'];
				}
			}
		}
		return $this->PrixMiniProgramme;
	}

	// Mixe les données lots et les données programmes
	function getPrgsLots(){
		$programmesLots = array();
		foreach($this->Programme['programmes']['data'] as $key =>$programme){
			$programmesLots[$programme['idproduit']] = $programme;
		}
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			$programmesLots[$lots['idprg']]['lots'][] = $lots;
		}

		return $programmesLots;
	}

	//Compte le nombre de logement dispo par programme
	function getNbreLogementDispo(){
		$logementsDispo = array();
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if($lots['statu_lot']=='libre'){
				if(!empty($logementsDispo[$lots['idprg']])){
					$logementsDispo[$lots['idprg']] = $logementsDispo[$lots['idprg']]+1;
				}else{
					$logementsDispo[$lots['idprg']] = 1;
				}
			}
		}

		return $logementsDispo;
	}

	//Compte le nombre de logement par programme
	function getNbreLogement(){
		$logements = array();
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if(!empty($logements[$lots['idprg']])){
				$logements[$lots['idprg']] = $logements[$lots['idprg']]+1;
			}else{
				$logements[$lots['idprg']] = 1;
			}
		}
		return $logements;
	}

	//Compte le nombre de logement dispo par programme par typologie 
	function getNbreLogementDispoByTypo(){
		$logementsDispoByTypo = array();
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if($lots['statu_lot']=='libre'){
				if(!empty($logementsDispoByTypo[$lots['idprg']][$lots['typologie']])){
					$logementsDispoByTypo[$lots['idprg']][$lots['typologie']] = $logementsDispoByTypo[$lots['idprg']][$lots['typologie']]+1;
				}else{
					$logementsDispoByTypo[$lots['idprg']][$lots['typologie']] = 1;
				}
			}
		}

		return $logementsDispoByTypo;
	}

	//Compte le nombre de logement total par programme par typologie 
	function getNbreLogementByTypo(){
		$logementsByTypo = array();
		foreach($this->Lots['lots']['data'] as $key =>$lots){
			if(!empty($logementsByTypo[$lots['idprg']][$lots['typologie']])){
				$logementsByTypo[$lots['idprg']][$lots['typologie']] = $logementsByTypo[$lots['idprg']][$lots['typologie']]+1;
			}else{
				$logementsByTypo[$lots['idprg']][$lots['typologie']] = 1;
			}
		}

		return $logementsByTypo;
	}

	// Récupère les plans des lots 
	function getPlanLot(){
		$plansLot = array();
		foreach($this->Medias['media']['data'] as $key =>$lots){
			if($lots['idcat']==$this->idMediaplan && $lots['idsscat']==$this->idMediaplan2d && !empty($lots['idlot'])){
				$plansLot[$lots['idproduit']][$lots['idlot']] = $lots['fileurl'];
			}
		}
		return $plansLot;
	}

	// Récupère les visites virtuelles 
	function getVisiteVirtuelle(){
		$visiteVirtuelleLot = array();
		foreach($this->Medias['media']['data'] as $key =>$lots){
			if($lots['idcat']==$this->idMediaInteractif && $lots['idsscat']==$this->idMediaVisiteVirtuelle && !empty($lots['typologie'])){
				$visiteVirtuelleLot[$lots['idproduit']][$lots['typologie']] = array('fileurl'=>$lots['fileurl'],'titre'=>$lots['titre']);
			}
		}
		return $visiteVirtuelleLot;
	}

	// Récupère les packages des lots
	function getPackageLot(){
		$packageLots = array();
		foreach($this->Packages['packages']['idlot'] as $idlot =>$package){
			$fullPack = explode('|',$package['lots']);
			$packageLots[$idlot] = $fullPack;
		}
		return $packageLots;
	}

	//Récupère les programmes à proximité en km
	function askNearProgramme($distance,$codePostal=null,$ville=null,$tri=null){
		$urlapi = URLIMMOLEAD.'api/1/'.$this->urlApiRayonnage.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY.'&distance='.$distance.'&ville='.$ville.'&codePostal='.$codePostal.'&tri='.$tri;
		$content = file_get_contents($urlapi);
		$content = json_decode($content,true);
		$this->nearProgrammes = $content;
	}

	// Envoi des médias dans Immolead
	function sendMedia($file_name,$idpartenaire){

		$url = URLIMMOLEAD.'api/1/'.$this->urlApiUpload.'?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY;
		$itef = 0;
	 
		foreach ($_FILES[$file_name] as $filek => $filev) {
			$cfile = curl_file_create($_FILES[$file_name]['tmp_name'][$itef],'image/png',$_FILES[$file_name]['name'][$itef]);
			// echo "cfile : ".$cfile;
			$postdatas = array();
			$postdatas["file"]=$cfile;
			$postdatas["idpartenaire"]=$idpartenaire;
			 

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL,$url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postdatas);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			$remoteurl = curl_exec($curl);
	    	curl_close($curl); 
			$itef++;
			if ($itef == count($_FILES[$file_name]['tmp_name'])) {
				return $remoteurl; 
			}
		}

	}

	// Retourne les données programmes brutes
	function getPrgs(){
		return $this->Programme;
	}

	// Retourne les données lots brutes
	function getLots(){
		return $this->Lots;
	}

	// Retourne les données programmes brutes de l'api programme du client
	function getRawPrgs(){
		return $this->ProgrammeClient;
	}

	// Retourne les données media brutes de l'api media
	function getRawMedias(){
		return $this->Medias;
	}

	// Retourne les programmes de proximité
	function getNearProgrammes(){
		return $this->nearProgrammes;
	}


}

?>