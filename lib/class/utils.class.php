<?php

class Utils
{

	private $urlApiNeolane = 'http://apineolane.immolink.fr/api-mki.php?apikey=mki-220777-Pui88ance';
	private $logDirectory = 'logs';
	private $logDirectoryNeolane = 'neolane';
	public $logDirectoryImmolead = 'immolead';
	private $logDirectoryMandrill = 'mandrill';
	private $logDirectoryMailchimp = 'mailchimp';
	private $logDirectorySMS = 'sms';
	private $apikeyMandrill = 'iFd9HJpPNDSZIHp2Lx29_A';
	private $senderEmail = 'info@sendcontact.immolink.fr';
	private $urlSendSMS = "http://sms.immo-lead.com/api/api-sms.php?apikey=12345mkio1:=55-&customerkey=mkiclapu13353_ce5";

	// Envoi une requête en POST sur l'API contact Immolead (API REST). Le flux envoyé doit être en JSON
	function sendjson($flux,$url,$password=null,$method=null){

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(!empty($password)){
	    	curl_setopt($ch, CURLOPT_USERPWD, "user:" . $password);
		}
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	        'Content-Type: application/json',
	        'Content-Length: '.strlen($flux)
	    ));
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    if($method=='POST'){
			curl_setopt($ch, CURLOPT_POST, 1);
		}elseif($method=='PUT'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		}elseif($method=='DELETE'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		}else{
			curl_setopt($ch, CURLOPT_POST, 1);
		}
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $flux);
	    $result = curl_exec($ch);
	    return $result;
	}

	// Envoi le flux vers Immolead et log l'envoi et le retour
	public function sendContactImmolead($flux){
		$this->logData($this->logDirectoryImmolead,$flux.'|');
		$url = URLIMMOLEAD.'api/1.0/contacts?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY;
		$result = $this->sendjson($flux,$url,'');
		$this->logData($this->logDirectoryImmolead,$result.PHP_EOL);
		return $result;
	}

	// Envoi le flux vers Neolane et log l'envoi et le retour
	public function sendContactNeolane($flux){
		$this->logData($this->logDirectoryNeolane,$flux.'|');
		$result = $this->sendjson($flux,$this->urlApiNeolane,'');
		$this->logData($this->logDirectoryNeolane,$result.PHP_EOL);
		return $result;
	}

	// Création du répertoires de logs s'il n'existe pas
	public function createLog(){
		// Répertoire Neolane
		if (!is_dir($this->logDirectory.'/'.$this->logDirectoryNeolane)) {
			mkdir($this->logDirectory.'/'.$this->logDirectoryNeolane, 0755, true);
		}

		// Répertoire Immolead
		if (!is_dir($this->logDirectory.'/'.$this->logDirectoryImmolead)) {
			mkdir($this->logDirectory.'/'.$this->logDirectoryImmolead, 0755, true);
		}

		// Répertoire Mandrill
		if (!is_dir($this->logDirectory.'/'.$this->logDirectoryMandrill)) {
			mkdir($this->logDirectory.'/'.$this->logDirectoryMandrill, 0755, true);
		}

		// Répertoire Mailchimp
		if (!is_dir($this->logDirectory.'/'.$this->logDirectoryMailchimp)) {
			mkdir($this->logDirectory.'/'.$this->logDirectoryMailchimp, 0755, true);
		}

		// Répertoire SMS
		if (!is_dir($this->logDirectory.'/'.$this->logDirectorySMS)) {
			mkdir($this->logDirectory.'/'.$this->logDirectorySMS, 0755, true);
		}
	}

	// Log les données dans un fichier. $directory = neolane ou immolead
	public function logData($directory,$dataToLog){
		$this->createLog();
		$fp = fopen($this->logDirectory.'/'.$directory.'/'.date(Ymd).'.txt', 'a');
		fwrite($fp, $dataToLog);
		fclose($fp);
	}
	
	// Envoi un email avec Mandrill
	public function sendEmail($replyto=null,$senderName=null,$emailDestinataire=null,$pj=array(),$sujet=null,$contentType='html',$message){

		$to = array();

		$destinataires = array('email' => '', 'name' => '', 'type' => 'to');
		$destinataires['email'] = $emailDestinataire;
		array_push($to, $destinataires);
			
		if(!empty($pj)){
			$attachments = array();
			for($i=0;$i<count($pj);$i++){
				$url_file = $pj[$i];
				$imagedata = file_get_contents($url_file);
				$baseimg = base64_encode($imagedata);
				$type_mime = mime_content_type($url_file);
				$attachment_pj = array(
	                'type' => $type_mime,
	                'name' => $pj[$i],
	                'content' => $baseimg
	            );
				array_push($attachments,$attachment_pj);
			}
		}

		try {
			$mandrill = new Mandrill($this->apikeyMandrill);
		
			$messageMD = array(
				'subject' => $sujet,
				'from_email' => $this->senderEmail,
				'from_name' => $senderName,
				'to' => $to,
				'headers' => array('Reply-To' => $replyto),
				'important' => false,
				'track_opens' => true,
				'track_clicks' => true,
				'auto_text' => true,
				'auto_html' => null,
				'inline_css' => null,
				'url_strip_qs' => null,
				'preserve_recipients' => null,
				'view_content_link' => null,
				'bcc_address' => '',
				'tracking_domain' => null,
				'signing_domain' => null,
				'return_path_domain' => null,
				'merge' => null,
				'tags' => null
			);
			if($contentype=='txt'){
				$messageMD['text'] = $message;
				$messageMD['html'] = "";
			}else{
				$messageMD['text'] = "";
				$messageMD['html'] = $message;
			}
			if(!empty($attachments)){
				$messageMD['attachments'] = $attachments;	
			}

			$this->logData($this->logDirectoryMandrill,json_encode($messageMD).'|');
			$async = false;
			$result = $mandrill->messages->send($messageMD, $async, $ip_pool, $send_at);
			$this->logData($this->logDirectoryMandrill,json_encode($result).PHP_EOL);
			return true;
			
		} catch(Mandrill_Error $e) {
			$this->logData($this->logDirectoryMandrill,get_class($e).'-'.$e->getMessage().PHP_EOL);
			return false;
		}
	}

	// Envoi un contact à Mailchimp dans une liste
	public function sendContactToMailchimp($email,$tags=array(),$mergeFields=array()){

		if(!empty($email)){
			$flux = array(
				'email_address' => $email,
				'status' => 'subscribed',
				'tags' => $tags
			);
			if(!empty($mergeFields)){
				$flux['merge_fields'] = array();
			}
			if(!empty($mergeFields['firstname'])){
				$flux['merge_fields']['FNAME'] = $mergeFields['firstname'];
			}
			if(!empty($mergeFields['lastname'])){
				$flux['merge_fields']['LNAME'] = $mergeFields['lastname'];
			}

			$this->logData($this->logDirectoryMailchimp,json_encode($flux).'|');
			$url = 'https://'.PREFIXURL_MC.'.api.mailchimp.com/3.0/lists/'.LISTID_MC.'/members';
			
			try {
				$result = $this->sendjson(json_encode($flux),$url,PASSWORD_MC);
				$this->logData($this->logDirectoryMailchimp,$result.PHP_EOL);
				$result = json_decode($result,true);
				if($result['id']){
					return true;
				}else{
					return false;
				}
			} catch(Exception $e) {
				$this->logData($this->logDirectoryMailchimp,get_class($e).'-'.$e->getMessage().PHP_EOL);
				return false;
			}
		}else{
			return false;
		}
	}

	
	/* 	Envoi un SMS
		idclient : obligatoire
		sender : nom en toute lettre
		phoneNumber : en à 10 chiffres commençant par 06 ou 07
		mode smspro ou test
		campagne pour définir une campagne
		origine pour définir l'origine
	*/
	public function sendSMS($idclient,$sender,$phoneNumber,$message,$mode='smspro',$campagne,$origine){

		if(!empty($idclient) && !empty($sender) && !empty($phoneNumber) && !empty($message)){
			$phoneNumber = '33'.substr($phoneNumber, 1);
			$params = array(
				"contact"=>array(
					"0"=>array(
						"idclient"=>$idclient,
						"numtel"=>$phoneNumber,
						"messagesms"=>$message
					)
				),
				"campagne"=>$campagne,
				"prestataire"=>"somobile",
				"sendersms"=>$sender,
				"origine"=>$origine,
				"mode"=>$mode
			);

			$this->logData($this->logDirectorySMS,json_encode($params).'|');
			$sendsms = $this->sendjson(json_encode($params),$this->urlSendSMS);
			$this->logData($this->logDirectorySMS,$sendsms.PHP_EOL);

			$sendsms = json_decode($sendsms,true);
			foreach ($sendsms as $idsms => $tabResult) {
				if(!empty($tabResult['statut'][0])){
					$retour = $tabResult['statut'][0];
				}
			}
			if(!empty($idsms) && $retour=='OK'){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}

?>	