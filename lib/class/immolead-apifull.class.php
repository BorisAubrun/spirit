<?php

class ImmoleadApiFull
{

	private $baseUrl = 'api/1.0/fullapi/';
	private $eventTable = 'evenements';

	public function __construct(){

	}

	// Retourne les droits de l'API Full
	function infoApi(){

	}

	// Annule une réservation
	function cancelRDV($idevent,$iduser,$date,$raison){
		if(!empty($idevent) and !empty($iduser) and !empty($date) and !empty($raison)){
			$params = array(
				"idusercancel"=>$iduser,
				"datecancel"=>$date,
				"raison"=>$raison
			);
			$url = URLIMMOLEAD.$this->baseUrl.$this->eventTable.'/'.$idevent.'/?apikey='.APIKEY.'&customerkey='.CUSTOMERKEY;
			$utils = new Utils();
			$utils->createLog();
			$utils->logData($utils->logDirectoryImmolead,json_encode($params).'|');
			$utils->logData($utils->logDirectoryImmolead,$url.'|');
			$retour = $utils->sendjson(json_encode($params),$url,'','PUT');
			$utils->logData($utils->logDirectoryImmolead,$retour.PHP_EOL);
			$retour = json_decode($retour,true);
			if(isset($retour['rows'])){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}	
}

?>