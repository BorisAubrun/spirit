<!DOCTYPE html>  
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spirit Partenaires</title>


    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!--caroussel-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    <!--end caroussel-->
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
 

</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#EA0D2B",
      "text": "#ffffff"
    },
    "button": {
      "background": "#ffffff",
      "text": "#ff0f00"
    }
  },
  "position": "bottom-right",
  "content": {
    "message": "En poursuivant votre navigation sur ce site, vous acceptez nos conditions générales d’utilisation et notamment que des cookies soient utilisés afin d’améliorer votre expérience d’utilisateur et de vous offrir des contenus personnalisés. Vous êtes par ailleurs informés que nous mettons en oeuvre un système de détection des bloqueurs de publicité sur ce site.",
    "dismiss": "J'ai compris",
    "link": "En savoir plus" 
  }
});
</script>
<div class="popContact ct">
    <div class="containerModal">
        <p>CONTACTEZ NOTRE CONSEILLER <strong>M. Loris MONTOYA</strong></p>
        <div class="contentContact" style="    margin-bottom: 40px;">
            <div>
                <p><i class="fas fa-envelope"></i></p>
                <a href="mailto:contact@spiritpartenaires.fr">partenaires@spirit.net</a>
            </div>
            <div>
                <p><i class="fas fa-phone"></i></p>
                <a href="tel:0618702599">06 18 70 25 99</a>
            </div>
        </div>
        <p>Pour les programmes de Provence <strong>Mme Geneviève IZZO</strong></p>
        <div class="contentContact">
            <div>
                <p><i class="fas fa-envelope"></i></p>
                <a href="mailto:gizzo@spirit.net">gizzo@spirit.net</a>
            </div>
               <div>
                <p><i class="fas fa-phone"></i></p>
                <a href="tel:0624676510">06 24 67 65 10</a>
            </div> 
        </div>
    </div>
</div>
     <!-- POP IN -->
<div class="popContact2 ct success"> 
  <div class="containerModal">
        <p>Votre compte est créé ! Spirit est heureux de vous accueillir parmi ses partenaires.<br>
Vous retrouverez votre identifiant et mot de passe par email après validation.<br> N'hésitez pas à contacter notre service à cette adresse partenaires@spirit.net pour toutes informations complémentaires.<strong></strong></p></p>
        
    </div>
</div>
<!-- / POP IN -->