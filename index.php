<?php require('cookie.php'); ?>
<?php require('lib/var.php'); ?>
<?php require('header.php'); ?>

<main>
    <?php include('header-top.php'); ?>
    <!-- <section class="refSlider">
        <div class="slider">
            <div class="slide slide1">
                <h2>Blanc-Mesnil</h2>
            </div>
            <div class="slide slide2">
                <h2>Chelles</h2>
            </div>
            <div class="slide slide3">
                <h2>La Garenne</h2>
            </div>
            <div class="slide slide4">
                <h2>Lognes</h2>
            </div>

        </div>
    </section> -->
    <section class="contentSubscribes">
        <article>
            <h2>Devenir partenaire</h2>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis dolores eius inventore molestiae pariatur quia veniam veritatis voluptatibus.</p> -->
            <!-- <button type="button" class="ctaSubscribes btnRed toRed">S'inscrire</button> -->
            <a href="inscription" target="_blank" class="ctaSubscribes btnRed toRed">S'inscrire</a>
        </article>
        <article>
            <h2>Déjà inscrit ?</h2>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis dolores eius inventore molestiae pariatur quia veniam veritatis voluptatibus.</p> -->
            <a href="https://immo-lead.com" target="_blank" class="ctaSubscribes btnRed toRed">Se connecter</a>
        </article>
    </section>
    <section class="resume">
        <div class="containerResume">
            <div class="vertical_slide">
                <img src="img/bg-groupe-xs.jpg" alt="Spirit Immobilier partenaire" class="visibleXs">
                <img src="img/bg-groupe.jpg" alt="Spirit Immobilier partenaire" class="visibleMd">
                <img src="img/bg-groupe-xs2.jpg" alt="Spirit Immobilier partenaire" class="visibleXs">
                <img src="img/bg-groupe2.jpg" alt="Spirit Immobilier partenaire" class="visibleMd">
                <img src="img/bg-groupe-xs3.jpg" alt="Spirit Immobilier partenaire" class="visibleXs">
                <img src="img/bg-groupe3.jpg" alt="Spirit Immobilier partenaire" class="visibleMd">
                <img src="img/bg-groupe-xs4.jpg" alt="Spirit Immobilier partenaire" class="visibleXs">
                <img src="img/bg-groupe4.jpg" alt="Spirit Immobilier partenaire" class="visibleMd">
                <img src="img/bg-groupe-xs5.jpg" alt="Spirit Immobilier partenaire" class="visibleXs">
                <img src="img/bg-groupe5.jpg" alt="Spirit Immobilier partenaire" class="visibleMd">
            </div>
            <div>
                <h2>Le groupe Spirit</h2>
                <p>Le Groupe Spirit est un acteur majeur et indépendant de l'immobilier depuis 1988, que ce soit au travers de ses activités de promotion immobilière (immobilier résidentiel, d'entreprise et commercial), d'investissement ou d'Asset Management, le Groupe est porté par sa passion de l'<span class="gras">immobilier</span>, son exigence de qualité ainsi que par sa curiosité permanente des évolutions du marché et de la société. </p>
                <p class="gras">Devenir partenaire de SPIRIT, c'est l'assurance :</p>
                <ul class="gras">
                  <li><span>■</span> d'une rémunération attractive</li>
                  <li><span>■</span> d'une grille de prix unique</li>
                  <li><span>■</span> d'un stock à jour en temps réel</li>
                  <li><span>■</span> d'une assitance 7j/7 pour vous aider à concrétiser vos transactions</li>
                </ul>
                <p class="gras">Rejoignez-nous !</p>
            </div> 
        </div>
    </section>
</main>

<?php require('footer.php'); ?>
