<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#ff4d4d",
      "text": "#ffffff"
    },
    "button": {
      "background": "#ffffff",
      "text": "#ff0f00"
    }
  },
  "position": "bottom-right",
  "content": {
    "message": "En poursuivant votre navigation sur ce site, vous acceptez nos conditions générales d’utilisation et notamment que des cookies soient utilisés afin d’améliorer votre expérience d’utilisateur et de vous offrir des contenus personnalisés. Vous êtes par ailleurs informés que nous mettons en oeuvre un s",
    "dismiss": "J'ai Compris",
    "link": "En Savoir Plus"
  }
});
</script>