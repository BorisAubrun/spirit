$(document).ready(function () { 


  $('.btncookie').click(function(){
    $('.cookie').hide();
  })
    // $('.slider').slick({
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     arrows: false,
    //     speed:1400,
    //     autoplay:true,
    //     autoplaySpeed: 4000,
    //     dots: true,
    //     fade:true,
    //     focusOnSelect: true,
    //     customPaging: function (slider, i) {
    //         return '<div class="dot"></div>';
    //     },
    // });

    $('.add').click(function(){
      var ite = 1;
        ite++;
      $('.mul').append(' <div class="blocForm multinput"><div class="divLabel"><label for="emailchimp'+ite+'">Abonné un collaborateur :</label></div><div class="divInput"><input type="text" name="emailchimp[]"></div> </div>');
    })

    $('.ctaContact').click(function () {
       $('.ct').addClass('open');
       $('.success').hide();
    });

    $('.popContact').click(function () {
        $('.ct').removeClass('open'); 
    });
    
    $('.containerModal').click(function (e) {
        e.stopPropagation();
    })


  // slick carousel vertical slide

  $('.vertical_slide').slick({
    autoplay: true,
    dots: false,
    vertical: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    verticalSwiping: true,
    prevArrow: false,
    nextArrow: false,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          infinite:true,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          customPaging: function (slider, i) {
              return '<div class="dot"></div>';
          },
        }
      }
    ]
  });
 
  function surligne(champ, erreur)
  {
     if(erreur)
        $('#email').addClass('error');
     else
        $('#email').removeClass('error');
       
  }
  function verifMail(champ)
  {
     var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
     if(!regex.test(champ))
     {
        surligne(champ, true);
        return false;
     }
     else
     {
        surligne(champ, false);
        return true;
     }
  } 
  $('#errorln').hide();
  $('#errornu').hide();

  $("fieldset[data-step]").hide();
   $("button.button-outline").hide();
    $("fieldset[data-step=1]").show();
    $("fieldset[data-step=2]").hide();
    $("fieldset[data-step=3]").hide();


    $('.btns1').click(function(){
      $('.step1').show();
      $('.etape').hide();

      $('.btns2').click(function(){
        // start
      var arrayRcs = [];
      var staticUrl = "https://test.immo-lead.com/api/1.0/fullapi/partenaires/?apikey=49866-72848-7f840-21e21-fe1ff-866f111&customerkey=eb9ea-ce741-3b709-c5204-01703-e44b360";
      var tableRcs = '&filter[]=siret,';
      var operator = 'eq,'; 
      var value = $('#rcs').val();
      var result = staticUrl + tableRcs + operator + value;
     
      $.ajax({ 
        url: result,
        dataType: 'json', 
        async: false, 
        success: function(data){ 
        arrayRcs.push(data);
        } 
      });

     var isValid = true;

      if ($('#rcs').val() == '') {
        isValid = false;
        $('#rcs').addClass('error');
        $('.popuprcs').fadeOut();
        // console.log('error vide');
      }  
      else if (arrayRcs[0] != null) { 
        $('.popuprcs').fadeIn();   
        $('#rcs').addClass('error');
        // console.log('error array');
        // console.log(arrayRcs[0]); 
      }
      else{
        $('.popuprcs').fadeOut();  
        $('#rcs').removeClass('error'); 
        // console.log(arrayRcs[0]); 
      }

      if ($('#societe').val()== ''){
          $('#societe').addClass('error');
          isValid = false; 
      } else{
        $('#societe').removeClass('error');
      }

      if ($('#adresse').val()== ''){
          $('#adresse').addClass('error');
          isValid = false; 
      } else{
        $('#adresse').removeClass('error');
      }

      if ($('#email').val()== ''){
              $('#email').addClass('error');
              isValid = false; 
      } else{
        $('#email').removeClass('error');
      }

      var str = $('#email').val();
      if(!verifMail(str)) {
          $('#email').addClass('error');
          isValid = false;
      } 

      if ($('#capital').val()== ''){
          $('#capital').addClass('error');
          isValid = false;
      } else{
        $('#capital').removeClass('error');
      }

      if ($('#lastname').val()== ''){
          $('#lastname').addClass('error');
          isValid = false;
      } else{
        $('#lastname').removeClass('error');
      } 

      if ($('#ncp').val() == ''){
          $('#ncp').addClass('error');
          isValid = false;
      } else{
        $('#ncp').removeClass('error');
      }      
      if ($('#fileToUploadkbis').val() == ""){
         $('#fileToUploadkbis').addClass('error');
          isValid = false;
      }else{ 
        $('#fileToUploadkbis').removeClass('error');
      }
      if ($('#fileToUploadct').val() == ""){
         $('#fileToUploadct').addClass('error');
          isValid = false;
      }else{ 
        $('#fileToUploadct').removeClass('error');
      }
      if ($('#fileToUploadag').val() == ""){
         $('#fileToUploadag').addClass('error');
          isValid = false;
      }else{ 
        $('#fileToUploada').removeClass('error');
      }
      if ($('#fileToUploada').val() == ""){
         $('#fileToUploada').addClass('error');
          isValid = false;
      }else{ 
        $('#fileToUploadag').removeClass('error');
      }
      if ($('#fileToUploadcig').val() == ""){
         $('#fileToUploadcig').addClass('error');
          isValid = false;
      }else{ 
        $('#fileToUploadcig').removeClass('error');
      }



      if (isValid && !$('input').hasClass('error') && !$('#rcs').hasClass('error')){
        $('.step2').show();
        $('.step1').hide();
  
        $('.btns3').click(function(){
      var arrayEmail = [];
             var staticUrl2 = "https://test.immo-lead.com/api/1.0/fullapi/utilisateurs/?apikey=49866-72848-7f840-21e21-fe1ff-866f111&customerkey=eb9ea-ce741-3b709-c5204-01703-e44b360";
              var tableEmail = '&filter[]=email,'; 
              var operator = 'eq,'; 
              var value2 = $("#email3").val();
              var result2 = staticUrl2 + tableEmail + operator + value2; 
                 $.ajax({ 
                url: result2,
                dataType: 'json', 
                async: false, 
                success: function(dataE){ 
                arrayEmail.push(dataE);
                } 
              }); 
                console.log(result2);
                console.log(arrayEmail);
          var isValid2 = true; 
          if ($('#nom').val()== ''){
              $('#nom').addClass('error');
              isValid2 = false; 
          }
          else{
            $('#nom').removeClass('error');
          } 

          if ($('#firstname').val()== ''){
              $('#firstname').addClass('error');
              isValid2 = false; 
          }
          else{
            $('#firstname').removeClass('error');
          } 

          if ($('#email3').val() == ''){
              $('#email3').addClass('error');
              isValid2 = false; 
             
          }
          else{
            $('#email3').removeClass('error');
          }
       
          // start2
          if ($('#email3').val() == '') {
            isValid = false; 
            $('#email3').addClass('error');
          }
          else if (arrayEmail[0] != null) {
            isValid = false;  
            $('.popupmail').fadeIn();
            $('#email3').addClass('error');
          }    
          else{
            $('.popupmail').fadeOut(); 
            $('#email3').removeClass('error'); 
          }
          var str2 = $('#email3').val();
          if(!verifMail(str2)) {
             $('#email3').addClass('error');
             isValid = false;
          } 
          if ($('#mobile').val()== ''){
              $('#mobile').addClass('error');
              isValid2 = false; 
          }
           else{
            $('#mobile').removeClass('error');
          }   
          if (isValid2 && !$('*').hasClass('error')) {  
            console.log('successfull');
             $('form').submit();  
          }
          // end2
        }) 
      }    
    }); 
  }) 
  $('.popuprcs').click(function(){
    $('.popuprcs').fadeOut();
  })
   $('.popupmail').click(function(){
    $('.popupmail').fadeOut();
  })
                      

});
